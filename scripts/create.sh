#!/usr/bin/env bash

SCRIPT_NAME=$1
echo $(date)
echo $(pwd)
touch $SCRIPT_NAME
echo "#!/usr/bin/env bash" >> $SCRIPT_NAME
chmod 755 $SCRIPT_NAME

