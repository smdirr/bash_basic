#!/usr/bin/env bash

MAX=$1
COUNTER=0

while [ $COUNTER -lt $MAX ]
do
  ((++COUNTER))
  echo $COUNTER
done

echo "Loop finished"


