#!/usr/bin/env bash

function Hello(){
  echo "Hello $1"
}

Goodbye() {
  echo "Goodbye $1"
}

echo "Calling the Hello function"
Hello Peter

echo "Calling the Goodbye function"
Goodbye Jane

