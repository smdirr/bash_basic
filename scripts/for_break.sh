#!/usr/bin/env bash

NAMES=$@

for NAME in $NAMES
do
  if [ $NAME = "Peter" ]
  then
    break
  fi

  echo "Hello $NAME"
done

echo "for loop terminated"


