#!/usr/bin/env bash

echo "The PATH is: $PATH"
echo "The terminal is: $TERM"
echo "The editor is: $ $EDITOR"

if [ -z $EDITOR ] # Empty string
then 
  echo "The editor is not set"
fi

PATH="/foo"
echo "The PATH is $PATH" # it does not affect environment vars
